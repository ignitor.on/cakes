<?php

namespace App\Mail;

use App\Models\Cakes;
use App\Models\Leads;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class newLead extends Mailable
{
    use Queueable, SerializesModels;

    public $lead;
    public $cake;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Leads $lead, Cakes $cake)
    {
      $this->lead = $lead;
      $this->cake = $cake;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Novo Bolo disponível');
        $this->to($this->lead->email);
        return $this->view('mail.newLead', [
          'lead' => $this->lead->email,
          'cake' => $this->cake->name
        ]);
    }
}
