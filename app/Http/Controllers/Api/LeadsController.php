<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Leads;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiControllerTrait;
use App\Mail\newLead;
use App\Models\Cakes;
use Illuminate\Support\Facades\Mail;

class LeadsController extends Controller
{

  use ApiControllerTrait {
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    try {
      $query = Leads::query();

      if ($request->has('email')) {
        $query->where('email', 'LIKE', '%' . $request->email . '%');
      }

      $leads = $query->orderBy('id', 'Desc')->paginate(20);

      return $this->createResponse([
        "leads" => $leads
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      $rulesInputsLeads = $this->getRulesInputsLeads();

      $validateLeads = $this->validateInputs($request, $rulesInputsLeads);
      $responseValidateLeads =  $validateLeads->original;

      if (isset($responseValidateLeads->error)) {
        return $validateLeads;
      }

      $cake = Cakes::find($request->cakes_id);

      if (!$cake) {
        return $this->createResponse([
          'message' => 'Bolo não encontrado.',
        ], 404);
      }

      $lead = [
        "email"    => $request->email,
        "cakes_id" => $request->cakes_id
      ];

      $lead = Leads::create($lead);

      if ($cake->amount > 0) {
        Mail::queue(new newLead($lead, $cake));
      }

      return $this->createResponse([
        "lead" => $lead
      ], 201);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    try {
      $lead = Leads::find($id);

      return $this->createResponse([
        "lead" => $lead
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      $rulesInputsLeads = $this->getRulesInputsLeads();

      $validateLeads = $this->validateInputs($request, $rulesInputsLeads);
      $responseValidateLeads =  $validateLeads->original;

      if (isset($responseValidateLeads->error)) {
        return $validateLeads;
      }

      $lead = Leads::find($id);

      if (!$lead) {
        return $this->createResponse([
          'message' => 'Lead não encontrado.',
        ], 404);
      }

      $lead->email   = $request->email;
      $lead->cakes_id = $request->cakes_id;
      $lead->update();

      return $this->createResponse([
        "lead" => $lead
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      $lead = Leads::find($id);

      if (!$lead) {
        return $this->createResponse([
          'message' => 'Lead não encontrado.',
        ], 422);
      }

      $lead->delete();

      return $this->createResponse([
        "message"  => 'Lead deletado com sucesso.',
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  public function getRulesInputsLeads()
  {
    return [
      'email'    => 'required',
      'cakes_id' => 'required',
    ];
  }
}
