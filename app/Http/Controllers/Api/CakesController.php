<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cakes;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiControllerTrait;

class CakesController extends Controller
{
  use ApiControllerTrait {}

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    try {
      $query = Cakes::query();

      if ($request->has('name')) {
        $query->where('name', 'LIKE', '%' . $request->name . '%');
      }

      $cakes = $query->orderBy('id', 'Desc')->paginate(20);

      return $this->createResponse([
        "cakes" => $cakes
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      $rulesInputsCakes = $this->getRulesInputsCakes();

      $validateCakes = $this->validateInputs($request, $rulesInputsCakes);
      $responseValidateCakes =  $validateCakes->original;

      if (isset($responseValidateCakes->error)) {
        return $validateCakes;
      }

      $cake = [
        "name"   => $request->name,
        "weight" => $request->weight,
        "price"  => $request->price,
        "amount" => $request->amount
      ];

      $cake = Cakes::create($cake);

      return $this->createResponse([
        "cake" => $cake
      ], 201);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    try {
      $cake = Cakes::find($id);

      return $this->createResponse([
        "cake" => $cake
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      $rulesInputsCakes = $this->getRulesInputsCakes();

      $validateCakes = $this->validateInputs($request, $rulesInputsCakes);
      $responseValidateCakes =  $validateCakes->original;

      if (isset($responseValidateCakes->error)) {
        return $validateCakes;
      }

      $cake = Cakes::find($id);

      if (!$cake) {
        return $this->createResponse([
          'message' => 'Bolo não encontrado.',
        ], 404);
      }

      $cake->name   = $request->name;
      $cake->weight = $request->weight;
      $cake->price  = $request->price;
      $cake->amount = $request->amount;


      $cake->update();

      return $this->createResponse([
        "cake" => $cake
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      $cake = Cakes::find($id);

      if (!$cake) {
        return $this->createResponse([
          'message' => 'Bolo não encontrado.',
        ], 422);
      }

      $cake->delete();

      return $this->createResponse([
        "message"  => 'Bolo deletado com sucesso.',
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  public function getRulesInputsCakes()
  {
    return [
      'name'   => 'required',
      'weight' => 'required',
      'price'  => 'required',
      'amount' => 'required'
    ];
  }
}
