<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Exception;
use App\Models\ProductQuotation;

trait ApiControllerTrait
{
    /**
     * <b>createResponse</b> Retorna o response em Json no formato: Resposta{conteudo[]}
     * Padronizando a apresentação dos dados e o formato do mesmo, para o mundo "externo"
     * OBS: Pode receber um objeto do Eloquent que herda da classe Illuminate\Pagination\AbstractPaginator
     * @param   $result
     * @return \Illuminate\Http\Response
     */

    protected function createResponse($result, $statusCode = null)
    {
       
        $response = (Object) $result;
        $statusCode = is_null($statusCode) ? app('Illuminate\Http\Response')->status() : $statusCode;
       

        if(property_exists($response, 'collects')){
   
            $data = (Array) $result->items();

            //Atributos da classe de paginação
            $data['links'] = [
             
                'first_page_url' => $response->url(1) ,
                'last_page_url'  => $response->url($response->lastPage()),
                'next_page_url'  => $response->nextPageUrl(),
                'prev_page_url'  => $response->previousPageUrl(),
              
            ];
            $data['meta'] = [

                'current_page'   => $response->currentPage(),
                'from'           => $response->firstItem(),
                'last_page'      => $response->lastPage(),
                'path'           => $response->resolveCurrentPath(),
                'per_page'       => $response->perPage(),
                'to'             => $response->lastItem(),
                'total'          => $response->total(),
            ];

        }
        else{
            $data = $response;
        }
            
        //retorna os dados em formato json
        return response()
        ->json($data, $statusCode, [], JSON_UNESCAPED_UNICODE)
        ->setStatusCode($statusCode);
    }


    public function validateInputs(Request $request, $rules, $messages = null)
    {      
        $validate = Validator::make($request->all(), $rules);

        if($validate->fails())
        {
            $errors['messages'] = $validate->errors();
            $errors['error']    = true;

            return $this->createResponse($errors, 422);
        }

        return $this->createResponse($validate);
    }
}